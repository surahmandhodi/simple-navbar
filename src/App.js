import logo from './logo.svg';
import './App.css';
import Routes from './routes/routes';
import { BrowserRouter as Router } from 'react-router-dom';
import Navbar from './layout/navbar';

function App() {
  return (
    <Router basename="/">
      <Navbar>
        <Routes />
      </Navbar>
    </Router>
  );
}

export default App;
