import React from 'react';
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import './navbar.css';


function Navbar({ children }) {
    const { Header, Content, Footer } = Layout;
    return (
        <Layout className="layout">
            <Header>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal">
                    <Menu.Item>
                        <Link to="/">Home</Link>
                    </Menu.Item>
                    <Menu.Item>
                        <Link to="/about">About</Link>
                    </Menu.Item>
                </Menu>
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <div className="site-layout-content">
                    {
                        children
                    }
                </div>
            </Content>
            <Footer className="text-center"></Footer>
        </Layout>
    );
}

export default Navbar;