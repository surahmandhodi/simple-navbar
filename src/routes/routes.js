import React, { Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import About from '../pages/about/about';
import Home from '../pages/home/home';

function Routes(props) {
    return (
        <>
            <Switch>
                <Suspense >
                    <Route
                        key="/"
                        path="/"
                        exact={true}
                        render={(props) => <Redirect exact from="/" to="/home" />}
                    />
                    <Route
                        key="/home"
                        path="/home"
                        exact={true}
                        render={(props) => <Home {...props} />}
                    />
                    <Route
                        key="/about"
                        path="/about"
                        exact={true}
                        render={(props) => <About {...props} />}
                    />
                </Suspense>
            </Switch>

        </>
    );
}

export default Routes;